'use strict';

var fs = require('fs');

module.exports = (name, obj) => {
    fs.exists(name, function (exists) {
        if (exists) {
            console.log('file exists');
        } else {
            var json = JSON.stringify(obj);
            fs.writeFile(name, json, 'utf8', function (res) {
                console.log('Write And Save Json Files set to ' + name);
            });
        }
    })
}