'use strict';

var faker = require('faker');

module.exports = () => {
    const database = { persons: [], accounts: [] };
    database.accounts.push({
        id: 1,
        email: 'admin@admin.com',
        userName: 'admin',
        passWord: 'admin'
    });
    for (var i = 2; i <= 50; i++) {        
        database.accounts.push({
            id: i,
            email: faker.internet.email(),
            userName: faker.internet.userName(),
            passWord: faker.internet.password()
        });
    }
    for (var i = 1; i <= 50; i++) {
        database.persons.push({
            id: i,
            firstName: faker.name.firstName(),
            surName: faker.name.lastName(),
            address: faker.address.streetAddress(),
            avatar: faker.image.avatar()
        });
    }
    return database
}