'use strict';
var fs           = require('fs');
var jsonServer   = require('json-server')
var jwt          = require('jsonwebtoken')
const bodyParser = require('body-parser')

const SECRET_KEY = '0123456789'
const expiresIn  = '1h'
const database   = JSON.parse(fs.readFileSync('./config/servers/db.json', 'UTF-8'))
// Add custom router
const router = jsonServer.router(database);

// Create a token from a payload 
function createToken(payload) {
    return jwt.sign(payload, SECRET_KEY, { expiresIn })
}

// Verify the token 
function verifyToken(token) {
    return jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ? decode : err)
}

// Check if the user exists in database
function isAuthenticated({ email, password }) {
    return database.accounts.findIndex(user => user.email === email && user.passWord === password) !== -1
}

// Add an express middleware
const middleware = (req, res, next) => {
    if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
        const status = 401
        const message = 'Bad authorization header'
        res.status(status).json({ status, message })
        return
    }
    try {
        verifyToken(req.headers.authorization.split(' ')[1])
        next()
    } catch (err) {
        const status = 401
        const message = 'Error: access_token is not valid'
        res.status(status).json({ status, message })
    }
};

// Init server
const server = jsonServer.create();

// Config bodyParser
server.use(jsonServer.bodyParser);

// DEFINE API CUSTOM
// POST: auth/login 
server.post('/api/auth/login', (req, res) => {   
    const { email, password } = req.body;
    if (isAuthenticated({ email, password }) === false) {
        const status = 401
        const message = 'Incorrect email or password'
        res.status(status).json({ response: { status, message } })
        return
    }
    const access_token = createToken({ email, password })
    res.status(200).json({ 'email': email, 'access_token': access_token })
})

// POST: auth/logout
server.post('/api/auth/logout', (req, res) => {
    res.status(200).json({ 'email': null });
})

server.use(jsonServer.bodyParser)
server.use('/api/auth/login', function (req, res, next) {
  next()
})

server.use('/api/persons', (req, res, next) => {
    middleware(req, res, next);
});
server.use('/api/accounts', (req, res, next) => {
    middleware(req, res, next);
});

// Add this before server.use(router)
server.use(jsonServer.rewriter({
    '/api/*': '/$1',
    '/persons': '/persons',
    '/persons/:id': '/persons?id=:id',
    '/accounts': '/accounts',
    '/accounts/:id': '/accounts?id=:id'
}))

// Use default router
server.use(router);

server.listen(3000, () => {
    console.log('JSON Server is running')
})
