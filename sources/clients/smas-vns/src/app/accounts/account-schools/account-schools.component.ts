import {
    Injector,
    OnInit,
    Component,
    ChangeDetectionStrategy,
    OnDestroy
  }                           from '@angular/core';

import { BaseComponent }    from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AccountsSandbox } from '../accounts.sandbox';

  @Component({
    selector: 'vns-account-schools',
    templateUrl: `./account-schools.component.html`,
    styleUrls: ['./account-schools.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  
  export class AccountSchoolsComponent extends BaseComponent implements OnInit, OnDestroy {

    private subscriptions:  Array<Subscription> = []; 
    
    constructor(
      injector: Injector,
      public AccountsSandbox: AccountsSandbox,
      private route: ActivatedRoute
    ) {
      super(injector);      
    }

    ngOnInit() {
      this.titlePage = 'AccountSchoolsComponent';
      this.setTitle(this.titlePage);     
    }    

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }    
}
  