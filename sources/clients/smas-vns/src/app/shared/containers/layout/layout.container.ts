import { Component, Input, ViewChild, ElementRef }        from '@angular/core';
import { Observable }       from 'rxjs/Observable';
import { Subscription }     from "rxjs";
import { LayoutSandbox }    from './layout.sandbox';
import { ConfigService }    from '../../../app-config.service';

@Component({
  selector: 'app-layout',
  templateUrl: `./layout.container.html`
})
export class LayoutContainer {

  public userImage:     string  = '';
  public userEmail:     string  = '';
  private assetsFolder: string;
  @Input() isNavbar: boolean;

  @ViewChild('wrapperContainer', { read: ElementRef }) mainWrapperContainer:ElementRef;

  private subscriptions: Array<Subscription> = [];

  constructor(
    private configService: ConfigService,
    public layoutSandbox:  LayoutSandbox
  ) {
    this.assetsFolder = this.configService.get('paths').userImageFolder;
  }

  ngOnInit() {
    this.registerEvents();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  private registerEvents() {
    // Subscribes to user changes
    this.subscriptions.push(this.layoutSandbox.user$.subscribe(user => {
      if (user) {
        this.userImage  = this.assetsFolder + 'user.jpg';
        this.userEmail  = localStorage.getItem('user')
      }
    }));
  }
}