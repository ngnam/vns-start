import { 
  Injectable
} 	                         from '@angular/core';
import { Router }            from '@angular/router';
import { Sandbox } 			     from '../../sandbox/base.sandbox';
import { Store }      	     from '@ngrx/store';
import * as store     	     from '../../store';
import * as authActions      from '../../store/actions/auth.action';
import * as settingsActions  from '../../store/actions/settings.action';
import * as menusActions     from '../../store/actions/menus.action';
import * as sbDataActions    from '../../store/actions/search-box.action';
import { TranslateService }  from 'ng2-translate';
import { Subscription }      from 'rxjs';

@Injectable()
export class LayoutSandbox extends Sandbox {

  public selectedLang$       = this.appState$.select(store.getSelectedLanguage);
  public availableLanguages$ = this.appState$.select(store.getAvailableLanguages);
  public user$               = this.appState$.select(store.getLoggedUser);
  public itemMenus$          = this.appState$.select(store.getMenusData);
  public searchBoxsData$     = this.appState$.select(store.getSearchBoxDatasData);
  private loginLoaded$;

  private subscriptions: Array<Subscription> = [];
  
  constructor(
    protected appState$: Store<store.State>,
    private translateService: TranslateService,
    private router: Router
  ) {
    super(appState$);
    this.registerEvents();
  }

  public selectLanguage(lang: any): void {
    this.appState$.dispatch(new settingsActions.SetLanguageAction(lang.code));
    this.appState$.dispatch(new settingsActions.SetCultureAction(lang.culture));
    this.translateService.use(lang.code);
  }

  public logout() {
    //this.router.navigate(['/login']);
    this.appState$.dispatch(new authActions.DoLogoutAction());
    this.subscribeToLoginChanges();
  }

  private subscribeToLoginChanges() {
    if (this.loginLoaded$) return;

    this.loginLoaded$ = this.appState$.select(store.getAuthLoaded)
    .subscribe(loaded => {
      if (!loaded) this.router.navigate(['/login'])
    });
  }

  /**
  * Loads Menus from the server
  */
  public loadMenus(): void {
    this.appState$.dispatch(new menusActions.LoadAction())
  }
  
  /**
   * Select menu header
   * @param e item menu clicked
   */
  public selectMenu(e: any): void {
    if(e) {
      let menu = e.itemData || {};
      if(menu && menu.url !== '' && menu.url !== undefined) {
        console.log(menu);
        this.router.navigate([ '/'+menu.url ]);
      }
    }
  }
  
    /**
  * Loads SearchBoxDatas from the server
  */
  public loadSearchBoxDatas(): void {
    this.appState$.dispatch(new sbDataActions.LoadAction())
  }

  /**
   * Unsubscribes from events
   */
  public unregisterEvents() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * Subscribes to events
   */
  private registerEvents(): void {
    this.loadMenus();
    this.loadSearchBoxDatas();
  }
}