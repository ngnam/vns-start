import {
    Component,
    ChangeDetectionStrategy,
    Renderer2,
    ViewChild
  }                            from '@angular/core';
import { TranslateService }    from 'ng2-translate';
import { SidebarMenuMock }     from '../../mocks';
import { DxTreeViewComponent } from 'devextreme-angular';
  
  @Component({
    selector: 'app-sidebar-menu',
    templateUrl: './sidebar-menu.component.html',
    styleUrls: ['./sidebar-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  export class SidebarMenuComponent {
    sidebarMenus: Array<any>;

    @ViewChild('sidebar') sidebarMenu: DxTreeViewComponent

    constructor(
      private translate: TranslateService) {
        this.sidebarMenus = SidebarMenuMock;
    }

    // selectItem = (e: any): void => {       
    //     this.redirectToRouter(e.itemData);
    //     this.setActiveMenu(e);
    // }

    // private setActiveMenu(e: any): void {
    //     let el = e.itemElement.offsetParent;    
    //     let parent: HTMLUListElement = this.renderer.parentNode(el);
    //     let listEl = parent.getElementsByTagName('li');   
    //     for (let item of Array.from(listEl)) {
    //         this.renderer.removeClass(item, 'current');
    //     }
    //     // set current menu 
    //     this.renderer.addClass(el, 'current');  
    // }

    // private redirectToRouter(item: any): void {
    //     if(item.url && item.queryParams) {
    //         this.router.navigate([item.url], { queryParams : item.queryParams });
    //     }else if(item.url) {
    //         this.router.navigate([item.url]);
    //     }
    // }
}
  