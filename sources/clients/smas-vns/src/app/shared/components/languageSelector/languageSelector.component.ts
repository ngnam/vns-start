import {
  Component,
  Output,
  Input,
  EventEmitter,
  ChangeDetectionStrategy,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'language-selector',
  templateUrl: './languageSelector.component.html',
  styleUrls: ['./languageSelector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    '(document:click)': 'onDocumentClick($event)'
  }
})
export class LanguageSelectorComponent {

  @Input() selectedLanguage: string;
  @Input() availableLanguages: Array<any>;
	@Output() select: EventEmitter<any> = new EventEmitter();

	public show: boolean = false;

  constructor(private elementRef: ElementRef) {}

  public onDocumentClick(event): void {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.show = false;
    }
  }

  public onToggle(): void {
    this.show = !this.show;
  }

  public selectLanguage(lang: any) {
    this.show = false;
    this.select.emit({code: lang.code, culture: lang.culture});
  }
}