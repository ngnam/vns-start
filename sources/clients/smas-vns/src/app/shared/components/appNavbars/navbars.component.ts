import {
    Component,
    Input,
    EventEmitter,
    ChangeDetectionStrategy
  }                             from '@angular/core';
import { Location }             from '@angular/common';

@Component({
    selector: 'app-navbars',
    templateUrl: './navbars.component.html',
    styleUrls: [`./navbars.component.scss`],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class NavbarComponent {    
   
    @Input() titlePage:             string;
    @Input() isBack:                boolean     = false;

    constructor(
        private location: Location) {}

    public goBack() {
        this.location.back();
    }
}