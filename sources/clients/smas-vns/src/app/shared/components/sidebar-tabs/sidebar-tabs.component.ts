import {
    Component,
    ChangeDetectionStrategy, 
  }                            from '@angular/core';
  
  @Component({
    selector: 'app-sidebar-tabs',
    templateUrl: './sidebar-tabs.component.html',
    styleUrls: ['./sidebar-tabs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  
  export class SidebarTabsComponent {

    selectedItems: any[] = [];
    allowDeleting: boolean = true;
    deleteType: string = 'static';
    tasks: string[];
    selectionMode: string = 'single';
    constructor() {
      this.tasks = ['task 1', 'task 2', 'task 3', 'task 4']
    }

    onItemClick(e: any):void {
      console.log(e);
    }

}
  