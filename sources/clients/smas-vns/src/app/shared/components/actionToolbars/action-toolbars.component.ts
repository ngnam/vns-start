import { SearchBoxsService } from './../../services/search-box.service';
import {
    Component,
    Output,
    Input,
    EventEmitter,
    ChangeDetectionStrategy,
  }                             from '@angular/core';

@Component({
  selector: 'app-action-toolbars',
  templateUrl: './action-toolbars.component.html',
  styleUrls: ['./action-toolbars.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ActionToolbarsComponent {

  @Input() isNew:           boolean = false;
  @Input() isDelete:        boolean = false;
  @Input() isSave:          boolean = false;

  @Output() onNew: EventEmitter<any> = new EventEmitter<any>();
  @Output() onDelete: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSave: EventEmitter<any> = new EventEmitter<any>();

  public onActionClick = {
    addNew: (e: any): void => {
      if(this.isNew) this.onNew.emit(e);
    },   
    delete: (e: any): void => {
      if(this.isDelete) this.onDelete.emit(e);
    },
    save: (e: any): void => {
      if(this.isSave) this.onSave.emit(e);
    }
  }  
}
