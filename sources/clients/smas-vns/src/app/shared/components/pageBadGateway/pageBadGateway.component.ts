import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'page-gate-way',
  templateUrl: `./pageBadGateway.component.html`,
  styleUrls: ['./pageBadGateway.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageBadGatewayComponent {
	constructor(private location: Location) {}

  public goBack() {
    this.location.back();
  }
}