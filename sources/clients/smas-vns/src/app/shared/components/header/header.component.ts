import {
  Component,
  Output,
  Input,
  EventEmitter,
  ChangeDetectionStrategy,
  Renderer2,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: `./header.component.html`,
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {

  @Input() selectedLanguage:    string;
  @Input() availableLanguages:  Array<any>;
  @Input() userImage:           string;
  @Input() userEmail:           string;  

  @Output() selectLanguage:     EventEmitter<any> = new EventEmitter();
  @Output() logout:             EventEmitter<any> = new EventEmitter();
  @Output() clickToggleNav:     EventEmitter<any> = new EventEmitter();

  // menu components
  @Input() itemMenus:           Array<any>;
  @Output() selectMenu:         EventEmitter<any> = new EventEmitter();

  // search box
  @Input() searchBoxsData:      Array<any>;

  isToggleNav:                    boolean = false;

  constructor(
    private renderer: Renderer2
  ){}

  public onToggleNav(isToggleNav, event) {
    this.isToggleNav = !isToggleNav;
    let elToggleNav: ElementRef;
    let path = event.path;
    if(path !== undefined) {
      // for chorme
      let elheader = path.find(item => item.className == 'header-container');
      elToggleNav = elheader.children.item(2);
    }else {
      // fix for firefox
      elToggleNav = event.target.offsetParent.children.item(0).children.item(2);
    }
    
    this.handlerToggleNav(elToggleNav);;
  }
  
  private handlerToggleNav(el: ElementRef): void {
    if(el && this.isToggleNav !== undefined) {
      if (!this.isToggleNav) {
        this.renderer.removeClass(el, 'toggle--nav');
      } else {
        this.renderer.addClass(el, 'toggle--nav');
      }
    }
  }
}
