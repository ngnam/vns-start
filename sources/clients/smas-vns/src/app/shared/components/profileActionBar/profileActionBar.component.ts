import { Component, Output, EventEmitter, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'profile-action-bar',
  templateUrl: `./profileActionBar.component.html`,
  styleUrls: ['./profileActionBar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileActionBarComponent {

  @Input() userImage: string;
  @Input() userEmail: string;
	@Output() logout: EventEmitter<any> = new EventEmitter();

  constructor() {}
}