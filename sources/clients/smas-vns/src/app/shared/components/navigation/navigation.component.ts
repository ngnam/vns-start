import {
  Component,
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Input,
  Output,
  Renderer2,
  ElementRef,
  AfterViewInit,
  EventEmitter,
  ViewChild
}                            from '@angular/core';
import { TranslateService }  from 'ng2-translate';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationComponent implements AfterViewInit {

  @Input() isNavbar: boolean;

  private _wrapperContainer:ElementRef;
  @Input() 
  set wrapperContainer(value: ElementRef ) {
    this._wrapperContainer = value;
  }

  constructor(
    private changeDetector: ChangeDetectorRef,
    private translate: TranslateService,
    private elementRef: ElementRef,
    private renderer: Renderer2) {
  }

  ngAfterViewInit(): void {
    /** 
     * Detaches the change detector from the change detector tree.
     */    
    if (!this.isNavbar) {
      this.setEfToggleNav(this._wrapperContainer);
    }  
  }

  toggleNavigator(isNavbar: boolean, el: any): void {    
    this.isNavbar = !isNavbar;
    let path = el.path;
    let elWrapper: ElementRef;
    if(path !== undefined) {
      // for chorme
      elWrapper = path.find(item => item.classList.contains('wrapper-main'));
    }else {
      // fix for firefox
      elWrapper = this.renderer.parentNode(el.target.offsetParent);
    }
    this.setEfToggleNav(elWrapper);
  }

  private setEfToggleNav(el: ElementRef): void {
    if(el && this.isNavbar !== undefined) {
      if (this.isNavbar) {
        this.renderer.removeClass(el, 'toggle--sidebar');
      } else {
        this.renderer.addClass(el, 'toggle--sidebar');
      }
    }
  }
}
