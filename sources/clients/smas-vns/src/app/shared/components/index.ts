import { NgModule }                     from '@angular/core';
import {
  FormsModule,
  ReactiveFormsModule
}                                       from '@angular/forms';
import { RouterModule }                 from '@angular/router';
import { CommonModule }                 from '@angular/common';

import { PipesModule }                  from '../pipes';
import { TranslateModule }              from 'ng2-translate';

import { SpinnerComponent }             from './spinner/spinner.component';
import { NavigationComponent }          from './navigation/navigation.component';
import { ProfileActionBarComponent }    from './profileActionBar/profileActionBar.component';
import { HeaderComponent }              from './header/header.component';
import { LanguageSelectorComponent }    from './languageSelector/languageSelector.component';
import { PageNotFoundComponent }        from './pageNotFound/pageNotFound.component';
import { PageBadGatewayComponent }      from './pageBadGateway/pageBadGateway.component';
import { MenuHeaderComponent }          from './menuHeader/menuHeader.component';
import { SearchBoxComponent }           from './searchBox/search-box.component';
import { NavbarComponent }              from './appNavbars/navbars.component';
import { ActionToolbarsComponent }      from './actionToolbars/action-toolbars.component';
import { PageContentComponent }         from './page-content/page.content.component';
import { SidebarMenuComponent }         from './sidebar-menu/sidebar-menu.component';
import { SidebarTabsComponent }         from './sidebar-tabs/sidebar-tabs.component';
import { 
  DxMenuModule, 
  DxPopoverModule, 
  DxSelectBoxModule, 
  DxToolbarModule,
  DxButtonModule,
  DxTreeViewModule,
  DxListModule
  }                                     from 'devextreme-angular';
  
export const COMPONENTS = [
  SpinnerComponent,
  NavigationComponent,
  ProfileActionBarComponent,
  HeaderComponent,
  MenuHeaderComponent,
  NavbarComponent,
  PageContentComponent,
  ActionToolbarsComponent,
  SearchBoxComponent,
  LanguageSelectorComponent,
  PageNotFoundComponent,
  PageBadGatewayComponent,
  SidebarMenuComponent,
  SidebarTabsComponent
];

export const MODULECONTROLS = [
  DxMenuModule,
  DxPopoverModule,
  DxSelectBoxModule,
  DxToolbarModule,
  DxButtonModule,
  DxTreeViewModule,
  DxListModule
];

@NgModule({
  imports: [
    RouterModule,
  	FormsModule,
  	ReactiveFormsModule,
  	CommonModule,
    TranslateModule,
    PipesModule,
    ...MODULECONTROLS
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ComponentsModule { }