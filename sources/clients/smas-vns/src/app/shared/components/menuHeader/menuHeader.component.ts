import {
    Component,
    Output,
    Input,
    EventEmitter,
    ChangeDetectionStrategy
  } from '@angular/core';
  
  @Component({
    selector: 'kiss-menu-header',
    templateUrl: `./menuHeader.component.html`,
    styleUrls: ['./menuHeader.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  export class MenuHeaderComponent {
    @Input() itemMenus:    Array<any>;  
    @Output() select:      EventEmitter<any> = new EventEmitter();

    public selectMenu(e) {
      this.select.emit(e);
    }
  }
  