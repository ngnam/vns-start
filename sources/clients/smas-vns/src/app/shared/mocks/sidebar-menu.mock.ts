import { SidebarMenuModel } from "../models";

export const SidebarMenuMock: SidebarMenuModel[] = [
    { id: 1, name: 'Phân quyền', iconFa: 'fa-dashboard', parentId: null, disabledClick: true },
    { id: 2, name: 'Quản lý nhân sự', iconFa: 'fa-dashboard', parentId: null, disabledClick: true },
    { id: 3, name: 'Quản lý học sinh', iconFa: 'fa-users', parentId: null, disabledClick: true},
    { id: 4, name: 'Quản lý môn học và điểm', iconFa: 'fa-cog', parentId: null, disabledClick: true },
    { id: 5, name: 'Theo dõi chất lượng h/s', iconFa: 'fa-cog', parentId: null, disabledClick: true },
    { id: 6, name: 'Thống kê báo cáo', iconFa: 'fa-cog', parentId: null, disabledClick: true },
    { id: 7, name: 'Quản lý tin nhắn', iconFa: 'fa-cog', parentId: null, disabledClick: true },
    { id: 8, name: 'Thông tin phản hồi', iconFa: 'fa-cog', parentId: null, disabledClick: true },
    { id: 9, name: 'Quản lý kì thi', iconFa: 'fa-cog', parentId: null, disabledClick: true },
    { id: 10, name: 'Quản lý năm học', iconFa: 'fa-cog', parentId: null, disabledClick: true },
    { id: 11, name: 'Quản lý tài khoản', iconFa: 'fa-cog', parentId: null, disabledClick: true },
    { id: 12, name: 'Lịch sử hoạt động', iconFa: 'fa-cog', parentId: null, disabledClick: true },

    { id: 21, name: 'Đơn vị trực thuộc', iconFa: 'fa-cog', parentId: 2, url: '/staffs/units', disabledClick: false },
    { id: 22, name: 'Danh sách nhân sự', iconFa: 'fa-cog', parentId: 2, url: '/staffs/hrm', disabledClick: false },

    { id: 31, name: 'Danh sách lớp', iconFa: 'fa-cog', parentId: 3, url: '/class/class-management', disabledClick: false },
    { id: 32, name: 'Danh sách học sinh', iconFa: 'fa-cog', parentId: 3, url: '/students/students-management', disabledClick: false },
    { id: 33, name: 'Thời khóa biếu', iconFa: 'fa-cog', parentId: 3, url: '/students/student-schedule', disabledClick: false },
    { id: 34, name: 'Điểm danh', iconFa: 'fa-cog', parentId: 3, url: '/absences/absence-management', disabledClick: false },
    { id: 35, name: 'Đánh giá nhận xét', iconFa: 'fa-cog', parentId: 3, url: '/review-mark/student-review-management', disabledClick: false },
    { id: 36, name: 'Xếp loại HL/HK', iconFa: 'fa-cog', parentId: 3, url: '/students/classification', disabledClick: false },
    { id: 37, name: 'Học bạ học sinh', iconFa: 'fa-cog', parentId: 3, url: '/students/student-report', disabledClick: false },
    { id: 38, name: 'Đăng ký sổ liên lạc điện tử', iconFa: 'fa-cog', parentId: 3, url: '/students/mobile-register', disabledClick: false },
    
    { id: 39, name: 'Danh sách môn học', iconFa: 'fa-cog', parentId: 4, url: '/subjects/subjects-management', disabledClick: false },
    { id: 40, name: 'Điểm môn học', iconFa: 'fa-cog', parentId: 4, url: '/students/mark-view', disabledClick: false },
    { id: 41, name: 'Điểm theo tuần', iconFa: 'fa-cog', parentId: 4, url: '/students/mark-view?q=week', disabledClick: false },
    { id: 42, name: 'Điểm kì thi theo lớp', iconFa: 'fa-cog', parentId: 4, url: '/marks/mark-exam', disabledClick: false },
    { id: 43, name: 'Điểm tổng kết', iconFa: 'fa-cog', parentId: 4, url: '/students/graduation-management', disabledClick: false },
    
    { id: 45, name: 'Nhận xét môn học', iconFa: 'fa-cog', parentId: 5, url: '/subjects/subject-review', disabledClick: false },
    { id: 46, name: 'Nhận xét chủ nhiệm', iconFa: 'fa-cog', parentId: 5, url: '/subjects/teacher-emarks', disabledClick: false },
    { id: 47, name: 'Điểm kiểm tra định kì', iconFa: 'fa-cog', parentId: 5, url: '/subjects/scope-1', disabledClick: false },
    { id: 48, name: 'Điểm kì thi theo lớp', iconFa: 'fa-cog', parentId: 5, url: '/subjects/scope-2', disabledClick: false },
    { id: 49, name: 'Điểm tổng kết', iconFa: 'fa-cog', parentId: 5, url: '/subjects/scope-3', disabledClick: false },
    
    { id: 50, name: 'Danh sách báo cáo', iconFa: 'fa-cog', parentId: 6, url: '/reports/reports-list', disabledClick: false },
    
    { id: 52, name: 'Gửi tin nhắn theo nhóm', iconFa: 'fa-cog', parentId: 7, url: '/messages/message-to-group', disabledClick: false },
    { id: 53, name: 'Gửi tin nhắn theo mẫu', iconFa: 'fa-cog', parentId: 7, url: '/messages/messages-frame', disabledClick: false },
    { id: 54, name: 'Lịch sử tin nhắn', iconFa: 'fa-cog', parentId: 7, url: '/messages/messages-log', disabledClick: false },
    { id: 55, name: 'DS/PH chưa nhận tin', iconFa: 'fa-cog', parentId: 7, url: '/messages/log1', disabledClick: false },
    { id: 56, name: 'DS/PH nhận nhiều tin', iconFa: 'fa-cog', parentId: 7, url: '/messages/log2', disabledClick: false },
    
    { id: 57, name: 'Ý kiến phản hồi', iconFa: 'fa-cog', parentId: 8, url: '/contacts/contact1', disabledClick: false },
    { id: 58, name: 'Phản hồi trực tuyến', iconFa: 'fa-cog', parentId: 8, url: '/contacts/contact2', disabledClick: false },

    { id: 59, name: 'Tạo mới kì thi', iconFa: 'fa-cog', parentId: 9, url: '/exams/create-exam', disabledClick: false },
    { id: 60, name: 'Danh sách kì thi', iconFa: 'fa-cog', parentId: 9, url: '/exams/exams-management', disabledClick: false },
    { id: 61, name: 'Điểm kì thi', iconFa: 'fa-cog', parentId: 9, url: '/exams/exam-scope', disabledClick: false },

    { id: 62, name: 'Tác vụ năm học', iconFa: 'fa-cog', parentId: 10, url: '/reports/report-1', disabledClick: false },
    { id: 63, name: 'Lịch sử lên lớp', iconFa: 'fa-cog', parentId: 10, url: '/reports/report-2', disabledClick: false },
    
    { id: 64, name: 'Tài khoản trong trường', iconFa: 'fa-cog', parentId: 11, url: '/accounts/account-schools', disabledClick: false },

    { id: 65, name: 'Xem lịch sử', iconFa: 'fa-cog', parentId: 12, url: '/logs/log1', disabledClick: false },
];