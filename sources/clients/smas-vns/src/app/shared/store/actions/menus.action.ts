import { Action }   from '@ngrx/store';
import { MenuModel }  from '../../models';
import { type }     from '../../utility';

export const ActionTypes = {
  SELECT:       type('[Menus] select'),
  LOAD:         type('[Menus] Load'),
  LOAD_SUCCESS: type('[Menus] Load Success'),
  LOAD_FAIL:    type('[Menus] Load Fail')
};

/**
 * Menu Action
 */
export class SelectAction implements Action {
    type = ActionTypes.SELECT;

    constructor(public payload: any = null) {}
}

 export class LoadAction implements Action {
     type = ActionTypes.LOAD;

     constructor(public payload: any = null) {}
 }

 export class LoadSuccessAction implements Action {
    type = ActionTypes.LOAD_SUCCESS;
  
    constructor(public payload: Array<MenuModel>) { }
  }
  
  export class LoadFailAction implements Action {
    type = ActionTypes.LOAD_FAIL;
  
    constructor(public payload: any = null) { }
  }
  
  export type Actions
    = SelectAction
    | LoadAction
    | LoadSuccessAction
    | LoadFailAction;
