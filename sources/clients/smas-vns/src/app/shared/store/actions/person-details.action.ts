import { Action }   from '@ngrx/store';
import { Person }  from '../../models';
import { type }     from '../../utility';

export const ActionTypes = {
  LOAD:         type('[Person Details] Load'),
  LOAD_SUCCESS: type('[Person Details] Load Success'),
  LOAD_FAIL:    type('[Person Details] Load Fail')
};

/**
 * Person Actions
 */
export class LoadAction implements Action {
  type = ActionTypes.LOAD;

  constructor(public payload: any = null) { }
}

export class LoadSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;

  constructor(public payload: Person) { }
}

export class LoadFailAction implements Action {
  type = ActionTypes.LOAD_FAIL;

  constructor(public payload: any = null) { }
}

export type Actions
  = LoadAction
  | LoadSuccessAction
  | LoadFailAction;