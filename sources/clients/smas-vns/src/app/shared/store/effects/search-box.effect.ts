import { getMenusData } from './../index';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable }             from '@angular/core';
import { Effect, Actions }        from '@ngrx/effects';
import { Action }                 from '@ngrx/store';
import { Observable }             from 'rxjs/Observable';
import { of }                     from 'rxjs/observable/of';
import { SearchBoxsService }      from '../../services';
import * as SearchBoxActions      from '../actions/search-box.action';
import { Store }                  from '@ngrx/store';
import * as store                 from '../index';
import { SearchBoxModel }         from '../../models';

@Injectable()
export class SearchBoxDatasEffects {

  constructor(
    private actions$: Actions,
    private searchBoxsService: SearchBoxsService,
    private appState$: Store<store.State>) {}

  /**
   * Menus list
   */
  @Effect()
  getSearchBoxDatas$: Observable<Action> = this.actions$
    .ofType(SearchBoxActions.ActionTypes.LOAD)
    .map((action: SearchBoxActions.LoadAction) => action.payload)
    .switchMap(state => {
      return this.searchBoxsService.getSearchBoxsData()
        .map(datas => new SearchBoxActions.LoadSuccessAction(datas))
        .catch(error  => Observable.of(new SearchBoxActions.LoadFailAction()));
    });
}