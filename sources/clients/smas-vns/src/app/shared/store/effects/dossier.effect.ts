import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable }             from '@angular/core';
import { Effect, Actions }        from '@ngrx/effects';
import { Action }                 from '@ngrx/store';
import { Observable }             from 'rxjs/Observable';
import { of }                     from 'rxjs/observable/of';
import { DossierApiClient }      from '../../../dossier-navigator/dossierApiClient.service';
import * as dossierAction       from '../actions/dossier.action';
import { Store }                  from '@ngrx/store';
import * as store                 from '../index';
import { 
    DossierNavigatorTreeModel 
    as TreeModel, DossierFilterModel }                from '../../models';

@Injectable()
export class DossierEffects {

  constructor(
    private actions$: Actions,
    private dossierApiClient: DossierApiClient,
    private appState$: Store<store.State>) {}

  /**
   * Trees list
   */
  @Effect()
  getTrees$: Observable<Action> = this.actions$
    .ofType(dossierAction.ActionTypes.LOAD)
    .map((action: dossierAction.LoadAction) => action.payload)
    .switchMap((state: DossierFilterModel) => {
      return this.dossierApiClient.getTrees(state.UserId, state.Active, state.Closed, state.Archived, state.IncludeGroup, state.IncludeGuest, state.IncludeTasks)
        .map(trees => new dossierAction.LoadSuccessAction(trees))
        .catch(error  => Observable.of(new dossierAction.LoadFailAction()));
    });
}
 