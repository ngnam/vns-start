import { getMenusData } from './../index';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable }             from '@angular/core';
import { Effect, Actions }        from '@ngrx/effects';
import { Action }                 from '@ngrx/store';
import { Observable }             from 'rxjs/Observable';
import { of }                     from 'rxjs/observable/of';
import { MenusService }           from '../../services';
import * as MenusActions          from '../actions/menus.action';
import { Store }                  from '@ngrx/store';
import * as store                 from '../index';
import { MenuModel }              from '../../models';

@Injectable()
export class MenusEffects {

  constructor(
    private actions$: Actions,
    private menuService: MenusService,
    private appState$: Store<store.State>) {}

  /**
   * Menus list
   */
  @Effect()
  getMenus$: Observable<Action> = this.actions$
    .ofType(MenusActions.ActionTypes.LOAD)
    .map((action: MenusActions.LoadAction) => action.payload)
    .switchMap(state => {
      return this.menuService.getDataMenus()
        .map(menus => new MenusActions.LoadSuccessAction(menus))
        .catch(error  => Observable.of(new MenusActions.LoadFailAction()));
    });
}