import { Observable }       from 'rxjs/Observable';
import { Injectable }       from '@angular/core';

import { SearchBoxModel }        from './../models';
import { SearchBoxsMock }        from '../mocks';

@Injectable()
export class SearchBoxsService {

    private SearchBoxSubscription;
    /**
    * Transforms grid data searchs box recieved from mockdata into array of 'SearchBoxModel' instances
    *
    */
    getSearchBoxsData(): Observable<SearchBoxModel[]> {
        return Observable.of(SearchBoxsMock);
    }
}