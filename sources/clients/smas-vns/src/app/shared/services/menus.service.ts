import { Observable }       from 'rxjs/Observable';
import { Injectable }       from '@angular/core';

import { MenuModel }        from './../models';
import { MenusMock }        from '../mocks';

@Injectable()
export class MenusService {

    private MenusSubscription;
    /**
    * Transforms grid data menus recieved from mockdata into array of 'MenuModel' instances
    *
    */
    getDataMenus(): Observable<MenuModel[]> {
        return Observable.of(MenusMock);
    }
}