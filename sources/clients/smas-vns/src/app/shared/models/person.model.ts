export class Person {
    public id:                  number;
    public firstName:           string;
    public surName:             string;
    public address?:            string;
    public avatar?:             Date;
    constructor(person: any = null) {
      this.id                 = person ? person.id || null : null;
      this.firstName          = person ? person.firstName || '' : null;
      this.surName            = person ? person.surName || '' : null;
      this.address            = person ? person.address || '' : null;
      this.avatar             = person ? person.avatar || null : null;
    }
  }