export class Account {
    public id:                  number;
    public email:               string;
    public userName:            string;
    public passWord:            string;
   
    constructor(account: any = null) {
      this.id                 = account ? account.id || null : null;
      this.email              = account ? account.email || '' : null;
      this.userName           = account ? account.userName || '' : null;
      this.passWord           = account ? account.passWord || '' : null;     
    }
  }