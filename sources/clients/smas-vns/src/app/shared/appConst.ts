import { InjectionToken } from "@angular/core";

export class AppConsts {
    static remoteServiceBaseUrl: string;
    static appBaseUrl: string;
}