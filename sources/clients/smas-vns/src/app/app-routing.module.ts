import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import { PageNotFoundComponent } from './shared/components/pageNotFound/pageNotFound.component';
import { PageBadGatewayComponent } from './shared/components/pageBadGateway/pageBadGateway.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', loadChildren: './home/dashboard.module#DashboardModule', data: { preload: true } },
  { path: 'absences', loadChildren: './absences/absences.module#AbsencesModule', data: { preload: true } },
  { path: 'accounts', loadChildren: './accounts/accounts.module#AccountsModule', data: { preload: true } },
  { path: 'class', loadChildren: './class/class.module#ClassModule', data: { preload: true } },
  { path: 'contacts', loadChildren: './contacts/contacts.module#ContactsModule', data: { preload: true } },
  { path: 'exams', loadChildren: './exams/exams.module#ExamsModule', data: { preload: true } },
  { path: 'logs', loadChildren: './logs/logs.module#LogsModule', data: { preload: true } },
  { path: 'marks', loadChildren: './marks/marks.module#MarksModule', data: { preload: true } },
  { path: 'messages', loadChildren: './messages/messages.module#MessagesModule', data: { preload: true } },
  { path: 'reports', loadChildren: './reports/reports.module#ReportsModule', data: { preload: true } },
  { path: '502', component: PageBadGatewayComponent, pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)    
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}