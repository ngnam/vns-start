import {
    Injector,
    OnInit,
    Component,
    ChangeDetectionStrategy,
    OnDestroy
  }                           from '@angular/core';

import { BaseComponent }    from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ExamsSandbox } from '../exams.sandbox';

  @Component({
    selector: 'vns-create-exam',
    templateUrl: `./create-exam.component.html`,
    styleUrls: ['./create-exam.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  
  export class CreateExamComponent extends BaseComponent implements OnInit, OnDestroy {

    private subscriptions:  Array<Subscription> = []; 
    
    constructor(
      injector: Injector,
      public examsSandbox: ExamsSandbox,
      private route: ActivatedRoute
    ) {
      super(injector);      
    }

    ngOnInit() {
      this.titlePage = 'CreateExamComponent';
      this.setTitle(this.titlePage);     
    }    

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }    
}
  