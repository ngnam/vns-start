import { CreateExamComponent } from './create-exam/create-exam.component';
import { ExamsComponent } from './exams/exams.component';
import { ExamScopeComponent } from './exam-scope/exam-scope.component';
import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { AuthGuard }                from '../shared/guards/auth.guard';
import { CanDeactivateGuard }       from '../shared/guards/canDeactivate.guard';

const routes: Routes = [
    {
        path: 'exams/create-exam',
        component: CreateExamComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'exams/exams-management',
        component: ExamsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'exams/exam-scope',
        component: ExamScopeComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ExamsRoutingModule { }
