import {
  Injector,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  Component
} from '@angular/core';

import { BaseComponent } from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ExamsSandbox } from '../exams.sandbox';

@Component({
  selector: 'vns-exam-scope',
  templateUrl: `./exam-scope.component.html`,
  styleUrls: ['./exam-scope.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ExamScopeComponent extends BaseComponent implements OnInit, OnDestroy {

  private subscriptions: Array<Subscription> = [];

  constructor(
    injector: Injector,
    public examsSandbox: ExamsSandbox,
    private route: ActivatedRoute
  ) {
    super(injector);
  }

  ngOnInit() {
    this.titlePage = 'ExamScopeComponent';
    this.setTitle(this.titlePage);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
