/**
 * Core Modules
 */
import { NgModule } from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { DevExtremeModule }         from 'devextreme-angular';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from 'ng2-translate';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

/**
 * Shared devextreme
 */
import { ComponentsModule }         from '../shared/components';
import { ContainersModule }         from '../shared/containers';

/**
 * Customs component, sandbox, routing modules
 */
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardSandbox } from './dashboard.sandbox';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
    imports: [       
        CommonModule,
        HttpClientModule,
        ComponentsModule,
        ContainersModule,
        TranslateModule,
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule,
        DevExtremeModule,
        DashboardRoutingModule
    ],
    declarations: [DashboardComponent],
    providers: [
        DashboardSandbox
    ],
})

export class DashboardModule { }
