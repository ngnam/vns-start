import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import { HttpClientModule }         from '@angular/common/http';
import { BrowserModule }            from "@angular/platform-browser";
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
import {
  ReactiveFormsModule,
  NG_VALIDATORS,
  FormControl
}                                   from "@angular/forms";
import { RouterModule }             from '@angular/router';
import { DossierRoutingModule }     from './dossier-routing.module';
import { DossierSandbox }           from './dossier.sandbox';
import { DossierApiClient }         from './dossierApiClient.service';
import { DossierService }           from './dossier.service';

import { TranslateModule }          from 'ng2-translate';

/**
 * Components
 */
import { DossierNavigatorComponent } from './dossiers/dossier-navigator.component';
import { TreeListComponent }         from './tree-list/tree-list.component';
import { NavFiltersComponent }       from './nav-filters/nav-filters.component';



/**
 * Dossier Modules
 */
import { ComponentsModule }         from '../shared/components';
import { ContainersModule }         from '../shared/containers';

/**
 * Module devextreme
 */
import { DevExtremeModule }         from 'devextreme-angular';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ComponentsModule,
    ContainersModule,
    DossierRoutingModule,
    TranslateModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    DevExtremeModule
  ],
  declarations: [
    DossierNavigatorComponent,
    TreeListComponent,
    NavFiltersComponent
  ],
  providers: [
    DossierSandbox,
    DossierService,
    DossierApiClient
  ]
})
export class DossierNavigatorModule {}
