import {
    Injectable,
    Inject,
    forwardRef
  }                                     from '@angular/core';
  import { 
    DossierNavigatorTreeModel as TreesModel 
    }                                   from './../shared/models';
  import { DossierSandbox }             from './dossier.sandbox';
  import { Observable }                 from 'rxjs/Observable';
  
  @Injectable()
  export class DossierService {
  
    private dossiersSubscription;
  
    /**
     * Transforms grid data trees recieved from the API into array of 'DossierNavigatorTreeModel' instances
     *
     * @param trees
     */
    static gridAdapter(trees: any): Array<any> {
      if(trees) trees[0].parentId = '0';
      return trees.map(tree => new TreesModel(tree));
    }
  }