import { Router }                 from '@angular/router';
import { getDossierTreesLoading } from './../shared/store/index';

import { Injectable }             from "@angular/core";
import { Store }                  from '@ngrx/store';
import { Subscription }           from "rxjs";
import { Sandbox }                from '../shared/sandbox/base.sandbox';
import { DossierApiClient }       from './dossierApiClient.service';
import * as store                 from '../shared/store';
import * as dossierActions        from '../shared/store/actions/dossier.action';
import {
  DossierNavigatorTreeModel as TreeModel, User, DossierFilterModel
}                                 from '../shared/models';
import { UtilService }            from "../shared/utility";

@Injectable()
export class DossierSandbox extends Sandbox {

  public dossierDatas$ = this.appState$.select(store.getDossierTreesData);
  public filterDossier$ = this.appState$.select(store.getFiltersDossier);
  public dossierLoading$ = this.appState$.select(store.getDossierTreesLoading);
  public loggedUser$ = this.appState$.select(store.getLoggedUser);

  private subscriptions: Array<Subscription> = [];

  constructor(
    private router: Router,
    protected appState$: Store<store.State>,
    private dossierApiClient: DossierApiClient,
    private utilService: UtilService,
  ) {
    super(appState$);
    this.registerEvents();
  }

  /**
* Loads dossier tree from the server
*/
  public loadDossierTrees(filters: any): void {
    this.appState$.dispatch(new dossierActions.LoadAction(filters));
  }

  // update filters
  OnFiltersChanged = {    
    onCkbActive: (e) => {
      this.filterDossier$.subscribe(filters => {
        filters.Active = e.value;
         this.appState$.dispatch(new dossierActions.SetVisibilityFilterAction.SetActiveAction(filters));
         this.appState$.dispatch(new dossierActions.LoadAction(filters));
      })
     
    },
    onCkbClosed: (e) => {
      this.filterDossier$.subscribe(filters => {
        filters.Closed = e.value;
        this.appState$.dispatch(new dossierActions.SetVisibilityFilterAction.SetClosedAction(filters));
        this.appState$.dispatch(new dossierActions.LoadAction(filters));
      })        
    },
    onCkbArchived: (e) => {
      this.filterDossier$.subscribe(filters => {
        filters.Archived = e.value;
        this.appState$.dispatch(new dossierActions.SetVisibilityFilterAction.SetClosedAction(filters));
        this.appState$.dispatch(new dossierActions.LoadAction(filters));
      })            
    },
    onCkbIncludeGroup: (e) => {
      this.filterDossier$.subscribe(filters => {
        filters.IncludeGroup = e.value;
        this.appState$.dispatch(new dossierActions.SetVisibilityFilterAction.SetIncludeGroupAction(filters));
        this.appState$.dispatch(new dossierActions.LoadAction(filters));
      })          
    },
    onCkbIncludeGuest: (e) => {
      this.filterDossier$.subscribe(filters => {
        filters.IncludeGuest = e.value;
        this.appState$.dispatch(new dossierActions.SetVisibilityFilterAction.SetIncludeGuestAction(filters));
        this.appState$.dispatch(new dossierActions.LoadAction(filters));
      })         
    },
    onCkbIncludeTasks: (e) => {
      this.filterDossier$.subscribe(filters => {
        filters.IncludeTasks = e.value;
        this.appState$.dispatch(new dossierActions.SetVisibilityFilterAction.SetIncludeGroupAction(filters));
        this.appState$.dispatch(new dossierActions.LoadAction(filters));
      })         
    },
  }

  /**
   * Unsubscribes from events
   */
  public unregisterEvents() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * Subscribes to events
   */
  private registerEvents(): void {
    // Subscribes to culture
    this.subscriptions.push(this.culture$.subscribe((culture: string) => this.culture = culture))      
  }
}
