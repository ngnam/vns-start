import {
    Component, 
    Input, 
    Output, 
    EventEmitter,
    ChangeDetectionStrategy,
    ViewChild,
    OnInit
  }                                     from '@angular/core';

  import { DossierNavigatorTreeModel as TreeModel } from './../../shared/models';

@Component({
    selector: 'app-tree-list',
    templateUrl: './tree-list.component.html',
    styleUrls: ['./tree-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class TreeListComponent implements OnInit {
   
    @Input()  TreesData:        Array<any>;
    
    constructor() {}     
    
    ngOnInit(): void {
    }  
}
  