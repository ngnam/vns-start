import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { AuthGuard }                from '../shared/guards/auth.guard';
import { CanDeactivateGuard }       from '../shared/guards/canDeactivate.guard';
import { DossierNavigatorComponent } from './dossiers/dossier-navigator.component';

const dossierNavRoutes: Routes = [
    {
        path: 'dossier',
        component: DossierNavigatorComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(dossierNavRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class DossierRoutingModule { }
