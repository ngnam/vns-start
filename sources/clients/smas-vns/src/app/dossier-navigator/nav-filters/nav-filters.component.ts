import {
    Component, 
    Input,
    Output, 
    EventEmitter,
    ChangeDetectionStrategy,
  }                                     from '@angular/core';
import { DossierFilterModel }              from '../../shared/models';

@Component({
    selector: 'dossier-nav-filters',
    templateUrl: './nav-filters.component.html',
    styleUrls: ['./nav-filters.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class NavFiltersComponent {

    @Input() filter:                DossierFilterModel = new DossierFilterModel();

    @Output() onValuedActive:       EventEmitter<any> = new EventEmitter();
    @Output() onValuedClosed:       EventEmitter<any> = new EventEmitter();
    @Output() onValuedArchived:     EventEmitter<any> = new EventEmitter();
    @Output() onValuedIncludeGroup: EventEmitter<any> = new EventEmitter();
    @Output() onValuedIncludeGuest: EventEmitter<any> = new EventEmitter();
    @Output() onValuedIncludeTasks: EventEmitter<any> = new EventEmitter();

    constructor() { }

    onFiltersChanged = {
        ckbActive: (e) => {
            this.onValuedActive.emit(e);
        },
        ckbClosed: (e) => {
            this.onValuedClosed.emit(e);
        },
        ckbArchived: (e) => {
            this.onValuedArchived.emit(e);
        },
        ckbIncludeGroup: (e) => {
            this.onValuedIncludeGroup.emit(e);
        },
        ckbIncludeGuest: (e) => {
            this.onValuedIncludeGuest.emit(e);
        },
        ckbIncludeTasks: (e) => {
            this.onValuedIncludeTasks.emit(e);
        }
    }

}
