import { 
  Injectable, QueryList
 }                          from '@angular/core';
import {
  HttpService,
  GET,
  Body,
  Path,
  Adapter,
  DefaultHeaders,
  Query,
  BaseUrl,
  BASEAPI
}                           from '../shared/asyncServices/http';
import { Observable }       from 'rxjs/Observable';
import { DossierService }   from './dossier.service';
import { HttpHeaders, HttpParams }      from '@angular/common/http';

import { DossierFilterModel } from '../shared/models';
import { environment } from '../../environments/environment';
import { AppConsts } from '../shared/appConst';

@Injectable()
@DefaultHeaders({
  'Accept': 'application/json',
  'Content-Type': 'application/json',
})

// api real
@BaseUrl(AppConsts.remoteServiceBaseUrl)

export class DossierApiClient extends HttpService {
  
  /**
   * Retrieves all TreesModel
   */
  @GET("api/fall-navigator/trees?UserId={UserId}&Active={Active}&Closed={Closed}&Archived={Archived}&IncludeGroup={IncludeGroup}&IncludeGuest={IncludeGuest}&IncludeTasks={IncludeTasks}")
  @Adapter(DossierService.gridAdapter)
  public getTrees(
    @Path('UserId') UserId?: any, 
    @Path('Active') Active?: any, 
    @Path('Closed') Closed?: any, 
    @Path('Archived') Archived?: any, 
    @Path('IncludeGroup') IncludeGroup?: any, 
    @Path('IncludeGuest') IncludeGuest?: any, 
    @Path('IncludeTasks') IncludeTasks?: any
  ): Observable<any> { 
    return null;
  };

}