import { NgModule }                  from '@angular/core';
import { CommonModule }              from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule
}                                    from '@angular/forms';
import { AuthRoutingModule }         from './auth-routing.module';
import { LoginComponent }            from './login/login.component';
import { ComponentsModule }          from '../shared/components';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule }           from 'ng2-translate';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { AuthSandbox }               from './auth.sandbox';
import { AuthApiClient }             from './authApiClient.service';
import { HttpClientModule }          from '@angular/common/http';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    BrowserAnimationsModule,
    TranslateModule,
    SimpleNotificationsModule
  ],
  declarations: [
    LoginComponent
  ],
  providers: [
    AuthApiClient,
    AuthSandbox   
  ]
})
export class AuthModule {}
