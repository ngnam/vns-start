import { Injectable }   from '@angular/core';
import { Observable }   from 'rxjs/Observable';
import {
  LoginForm, User
}                       from '../shared/models';
import {
  HttpService,
  POST,
  Body,
  DefaultHeaders,
  Adapter,
  Path,
  Query
}                       from '../shared/asyncServices/http';
import { AuthSandbox }  from './auth.sandbox';
import { parseHttpResponse } from 'selenium-webdriver/http';
import { paramBuilder } from '../shared/asyncServices/http/utils.service';
import { HttpHeaders } from '@angular/common/http';

@DefaultHeaders({
  'Accept': 'application/json',
  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' // 'application/json'
})

@Injectable()

export class AuthApiClient extends HttpService {
  
  /**
   * Submits login form to the server
   * 
   * @param form
   */  

  // @POST('connect/token')
  @Adapter(AuthSandbox.authAdapter)
  public login(searchParams: any): Observable<any> { 
    const options = {
      headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'})
    }
    return this.httpCLient.post(`${this.getConfigsApi().baseUrlToken}/connect/token`, searchParams, options);
  }

  /**
   * Logs out current user
   */
  //@POST("auth/logout") // call api logout
  @Adapter(AuthSandbox.logoutAdapter)
  public logout(): Observable<any> { return Observable.of(new User()) };
}