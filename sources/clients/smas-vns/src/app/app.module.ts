import { MessagesModule } from './messages/messages.module';
import { MarksModule } from './marks/marks.module';
import { ExamsModule } from './exams/exams.module';
import { ContactsModule } from './contacts/contacts.module';
import { AccountsModule } from './accounts/accounts.module';
import { AbsencesModule } from './absences/absences.module';
// Angular core modules
// tslint:disable-next-line:import-spacing
import { BrowserModule, Title }       from '@angular/platform-browser';
import {
  NgModule,
  APP_INITIALIZER
// tslint:disable-next-line:import-spacing
}                              from '@angular/core';
import { FormsModule }         from '@angular/forms';
import { 
  HttpModule,
  RequestOptions,
  XHRBackend,
  Http
}                              from '@angular/http';
import { Router }              from '@angular/router';

// Routes
import { AppRoutingModule }    from './app-routing.module';

// Modules
import { AppComponent }        from './app.component';
import { AuthModule }          from './auth/auth.module';
import { PersonsModule }       from './persons/persons.module';
import { 
  DossierNavigatorModule 
}                              from './dossier-navigator/dossier-navigator.module';
import { DashboardModule }     from './home/dashboard.module';
import { HttpServiceModule }   from './shared/asyncServices/http/http.module';
import { UtilityModule}        from './shared/utility';

// Store
import { store }               from './shared/store';

// Effects
import { AuthEffects }         from './shared/store/effects/auth.effect';
import { PersonsEffects }      from './shared/store/effects/persons.effect';
import { MenusEffects }        from './shared/store/effects/menus.effect';
import { 
         SearchBoxDatasEffects 
}                              from './shared/store/effects/search-box.effect';
import { DossierEffects }      from './shared/store/effects/dossier.effect';

// Guards
import { AuthGuard }           from './shared/guards/auth.guard';
import { CanDeactivateGuard }  from './shared/guards/canDeactivate.guard';

// Services
import { ConfigService }       from './app-config.service';

// Third party libraries
// tslint:disable-next-line:import-spacing
import { StoreModule }         from '@ngrx/store';
// tslint:disable-next-line:import-spacing
import { EffectsModule }       from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  TranslateModule,
  TranslateLoader,
  TranslateStaticLoader
// tslint:disable-next-line:import-spacing
}                              from 'ng2-translate';
import { TranslateService }    from 'ng2-translate';
import {
  SimpleNotificationsModule,
  NotificationsService
// tslint:disable-next-line:import-spacing
}                              from 'angular2-notifications';
import { ClassModule } from './class/class.module';
import { LogsModule } from './logs/logs.module';
import { ReportsModule } from './reports/reports.module';

/**
 * Calling functions or calling new is not supported in metadata when using AoT.
 * The work-around is to introduce an exported function.
 *
 * The reason for this limitation is that the AoT compiler needs to generate the code that calls the factory
 * and there is no way to import a lambda from a module, you can only import an exported symbol.
 */

export function configServiceFactory (config: ConfigService) {
  return () => config.load()
}


@NgModule({
  declarations: [
   AppComponent
  ],
  imports: [
    // Angular core dependencies
    BrowserModule,
    FormsModule,
    HttpModule,

    // Third party modules
    TranslateModule.forRoot(),
    SimpleNotificationsModule.forRoot(),

    // App custom dependencies
    HttpServiceModule.forRoot(),
    UtilityModule.forRoot(),
    PersonsModule,
    DossierNavigatorModule,
    DashboardModule,
    AbsencesModule,
    AccountsModule,
    ClassModule,
    ContactsModule,
    ExamsModule,
    LogsModule,
    MarksModule,
    MessagesModule,
    ReportsModule,
    AuthModule,
    AppRoutingModule,
  
    /**
     * StoreModule.provideStore is imported once in the root module, accepting a reducer
     * function or object map of reducer functions. If passed an object of
     * store, combineReducers will be run creating your application
     * meta-reducer. This returns all providers for an @ngrx/store
     * based application.
     */
    StoreModule.provideStore(store),// provideStore(store),

    /**
     * Store devtools instrument the store retaining past versions of state
     * and recalculating new states. This enables powerful time-travel
     * debugging.
     * 
     * To use the debugger, install the Redux Devtools extension for either
     * Chrome or Firefox
     * 
     * See: https://github.com/zalmoxisus/redux-devtools-extension
     */
    StoreDevtoolsModule.instrumentOnlyWithExtension(),//instrumentOnlyWithExtension(),

    /**
     * EffectsModule.run() sets up the effects class to be initialized
     * immediately when the application starts.
     *
     * See: https://github.com/ngrx/effects/blob/master/docs/api.md#run
     */
    EffectsModule.run(AuthEffects),
    EffectsModule.run(MenusEffects),
    EffectsModule.run(SearchBoxDatasEffects),
    EffectsModule.run(DossierEffects),
    EffectsModule.run(PersonsEffects),   
  ],
  providers: [
    Title,
    AuthGuard,
    CanDeactivateGuard,
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configServiceFactory,
      deps: [ConfigService], 
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }