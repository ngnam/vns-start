import {
    Injector,
    OnInit,
    ChangeDetectionStrategy,
    OnDestroy,
    Component
  }                           from '@angular/core';

import { BaseComponent }    from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MarksSandbox } from '../marks.sandbox';

@Component({
  selector: 'vns-marks',
  templateUrl: `./marks.component.html`,
  styleUrls: ['./marks.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
  
  export class MarksComponent extends BaseComponent implements OnInit, OnDestroy {

    private subscriptions:  Array<Subscription> = []; 
    
    constructor(
      injector: Injector,
      public marksSandbox: MarksSandbox,
      private route: ActivatedRoute
    ) {
      super(injector);      
    }

    ngOnInit() {
      this.titlePage = 'MarksComponent';
      this.setTitle(this.titlePage);     
    }    

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }    
}
  