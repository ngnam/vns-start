import {
    Injector,
    OnInit,
    Component,
    ChangeDetectionStrategy,
    OnDestroy
  }                           from '@angular/core';

import { BaseComponent }    from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AbsencesSandbox } from '../absences.sandbox';

  @Component({
    selector: 'vns-absences',
    templateUrl: `./absences.component.html`,
    styleUrls: ['./absences.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  
  export class AbsencesComponent extends BaseComponent implements OnInit, OnDestroy {

    private subscriptions:  Array<Subscription> = []; 
    
    constructor(
      injector: Injector,
      public absencesSandbox: AbsencesSandbox,
      private route: ActivatedRoute
    ) {
      super(injector);      
    }

    ngOnInit() {
      this.titlePage = 'Điểm danh';
      this.setTitle(this.titlePage);     
    }    

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }    
}
  