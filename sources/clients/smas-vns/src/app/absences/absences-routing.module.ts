import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { AuthGuard }                from '../shared/guards/auth.guard';
import { CanDeactivateGuard }       from '../shared/guards/canDeactivate.guard';
import { AbsencesComponent }        from './absences/absences.component';

const routes: Routes = [
    {
        path: 'absences/absence-management',
        component: AbsencesComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AbsencesRoutingModule { }
