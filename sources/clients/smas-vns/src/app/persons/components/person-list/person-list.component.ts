import {
  Component, 
  Input, 
  Output, 
  EventEmitter,
  ChangeDetectionStrategy,
  ViewChild
}                          from '@angular/core';

import { Person }          from '../../../shared/models';
import { DxFormComponent } from 'devextreme-angular';

@Component({
  selector:        'app-person-list',
  templateUrl:     './person-list.component.html',
  styleUrls:       ['./person-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonListComponent {

  @Input()  personsData:        Array<Person>;
  @Output() onRowClick:         EventEmitter<any> = new EventEmitter<any>();
  
  constructor() {}     

  public onSelect(e): void {
    this.onRowClick.emit(e);
  }    
}
