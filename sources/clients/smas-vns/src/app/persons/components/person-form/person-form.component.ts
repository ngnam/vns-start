  import {
    Component, 
    Input, 
    Output, 
    EventEmitter,
    ChangeDetectionStrategy,
    ViewChild
  }                          from '@angular/core';

  import { Person }          from '../../../shared/models';
  import { DxFormComponent } from 'devextreme-angular';
  
  @Component({
    selector:        'app-person-form',
    templateUrl:     './person-form.component.html',
    styleUrls:       ['./person-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  export class PersonFormComponent {

    @Input()  formData:         Person;
    @ViewChild('personForm')
    public personForm:DxFormComponent;

    colCountByScreen: Object;
    constructor() {
      this.colCountByScreen = {
        xs: 1,
        sm: 3,
        md: 3,
        lg: 3
      };
    }    

    public getSizeScreen(width) {
      if (width < 768) return "xs";
      if (width < 992) return "sm";
      if (width < 1200) return "md";
      return "lg";
    }  
  }
  