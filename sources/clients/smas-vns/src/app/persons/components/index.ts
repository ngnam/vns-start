
import { NgModule }               from '@angular/core';
import { PersonFormComponent }    from './person-form/person-form.component';
import { PersonListComponent }    from './person-list/person-list.component';
import { DevExtremeModule }       from 'devextreme-angular';
import { TranslateModule }        from 'ng2-translate';

// components
const COMPONENTS = [
    PersonFormComponent,
    PersonListComponent
];                                    

@NgModule({
  imports: [
    DevExtremeModule,
    TranslateModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class PersonsComponentsModule { }
