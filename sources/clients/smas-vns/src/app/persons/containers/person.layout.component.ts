  import {
    Component, 
    ChangeDetectionStrategy
  }                          from '@angular/core';

  @Component({
    selector:        'app-person-layout',
    templateUrl:     './person.layout.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  
  export class PersonLayoutComponent {    
    constructor() {}
  }
    