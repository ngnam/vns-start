import { NgModule } 				from '@angular/core';
import { TranslateModule }          from 'ng2-translate';
import { ContainersModule }         from '../../shared/containers';
import { PersonLayoutComponent }    from './person.layout.component';
import { PersonsComponentsModule }  from '../components';

export const CONTAINERS = [
    PersonLayoutComponent
];

@NgModule({
  imports: [
      PersonsComponentsModule,
      ContainersModule,
      TranslateModule,
  ],
  declarations: CONTAINERS,
  exports: CONTAINERS
})

export class PersonsContainersModule { }
