import { Person } from './../shared/models/person.model';
import { Injectable }       from '@angular/core';
import {
  HttpService,
  GET,
  PUT,
  POST,
  DELETE,
  Body,
  Path,
  Adapter,
  DefaultHeaders,
  BaseUrl
}                           from '../shared/asyncServices/http';
import { Observable }       from 'rxjs/Observable';
import { PersonsService }   from './persons.service';
import { HttpHeaders }      from '@angular/common/http';

@Injectable()
@DefaultHeaders({
  'Accept': 'application/json',
  'Content-Type': 'application/json',
})

@BaseUrl('http://192.168.1.1')

export class PersonsApiClient extends HttpService {
  
  /**
   * Retrieves all persons
   */
 
  @GET("api/persons")
  @Adapter(PersonsService.gridAdapter)
  public getPersons(): Observable<any> { 
    return null;
  };

  /**
   * Retrieves person details by a given id
   * 
   * @param id
   */
  @GET("api/persons/{id}")
  @Adapter(PersonsService.personDetailsAdapter)
  public getPersonDetails(@Path("id") id: number): Observable<any> { return null; };

  /**
   * Submits person update by a given id
   * 
   * @param id, 
   * @param person
   */
  @PUT("api/persons/{id}")
  @Adapter(PersonsService.updatePersonAdapter)
  public updatePerson(@Path("id") id: any, @Body person: Person): Observable<any> { return null; }

  /**
   * Submits login form to the server
   * 
   * @param person
   */
  @POST("api/persons") 
  @Adapter(PersonsService.insertPersonAdapter)
  public insertPerson(@Body person: Person): Observable<any> { return null; };

  /**
   * Delete person by a given id
   * 
   * @param id
   */
  @DELETE("api/persons/{id}") 
  @Adapter(PersonsService.deletePersonAdapter)
  public deletePerson(@Path("id") id: number): Observable<any> { return null; };
  
}