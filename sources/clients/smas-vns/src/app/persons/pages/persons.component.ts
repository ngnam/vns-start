import {
    Injector,
    OnInit,
    Component,
    ChangeDetectionStrategy
  }                           from '@angular/core';
  import { Router }           from '@angular/router';
  import { Subscription }     from "rxjs";
  import { PersonsSandbox }   from '../persons.sandbox';
  import { Person }           from '../../shared/models';
  import { BaseComponent }    from '../../shared/components/base.component';
  
  @Component({
    selector: 'app-persons',
    templateUrl: `./persons.component.html`,
    styleUrls: ['./persons.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  export class PersonsComponent extends BaseComponent implements OnInit {
    constructor(
      injector: Injector,
      private router: Router,
      public personsSandbox: PersonsSandbox
    ) {
      super(injector);
    }

    ngOnInit() {
      this.titlePage = 'Page manager persons';
      this.setTitle(this.titlePage);
    }
  
    /**
     * Callback function for grid select event
     * 
     * @param selected
     */
    public onSelect(e): void {
      this.personsSandbox.selectPerson(e.data);
      this.router.navigate(['/persons', e.data.id]);
    }

    onToolbarAction = {
      onAddNew: (url): any => {     
        this.router.navigate([url]);
      }     
    }
  }
  