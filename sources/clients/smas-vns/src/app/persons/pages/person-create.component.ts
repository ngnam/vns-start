import {
    Injector,
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    ViewChild,
    ViewChildren
  }                         from '@angular/core';
  import { ActivatedRoute, Router } from '@angular/router';
  import { Subscription }   from "rxjs";
  import { PersonsSandbox } from '../persons.sandbox';
  import { Person }         from '../../shared/models';
  import { BaseComponent }  from '../../shared/components/base.component';
  import { PersonFormComponent } from '../components/person-form/person-form.component';
  
  
  @Component({
    selector: 'app-person-create',
    templateUrl: './person-create.component.html',
    styles: ['./person-create.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  
  export class PersonCreateComponent extends BaseComponent implements OnDestroy {
    public person:          Person;
    private subscriptions:  Array<Subscription> = [];  
    
    @ViewChild(PersonFormComponent) formComponent: PersonFormComponent;

    constructor(
      injector: Injector,
      public personsSandbox: PersonsSandbox,
      private changeDetector: ChangeDetectorRef,
      private router: Router,
      ) {
      super(injector);
      this.registerEvents();
    }
  
    ngOnDestroy() {
      this.unregisterEvents();
    }
  
    /**
     * Unsubscribes from events
     */
    public unregisterEvents() {
      this.subscriptions.forEach(sub => sub.unsubscribe());
    }
  
    /*
    * Registers events
    */
    private registerEvents(): void {
      this.person = new Person();
      this.titlePage = `Create Person`;
      this.setTitle(this.titlePage);    
    }
  
    onToolbarAction = {
      onSave: (model: any): any => {
        // validate form
        let validateForm = this.formComponent.personForm.instance.validate();
        if (!validateForm.isValid) {
          let message = validateForm.brokenRules.map(o => o.message).join(', ');
          this.personsSandbox.notifyMessage(message, 'error');
        } else {
          this.personsSandbox.createPerson(model);
          this.personsSandbox.loadPersons();
          this.personsSandbox.notifyMessage('Success!', 'success');
        }         
      }
    }
  
  }
  