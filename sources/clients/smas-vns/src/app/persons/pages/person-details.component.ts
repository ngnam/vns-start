import {
  Injector,
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild
}                         from '@angular/core';
import { 
  Router, 
  ActivatedRoute 
}                          from '@angular/router';
import { Subscription }    from "rxjs";
import { PersonsSandbox }  from './../persons.sandbox';
import { Person }          from '../../shared/models';
import { BaseComponent }   from '../../shared/components/base.component';
import { PersonFormComponent } from '../components/person-form/person-form.component';


@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styles: ['./person-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonDetailsComponent extends BaseComponent implements OnDestroy {
  public person:          Person;
  private subscriptions:  Array<Subscription> = [];  

  @ViewChild(PersonFormComponent) formComponent: PersonFormComponent;

  constructor(
    injector: Injector,
    public personsSandbox: PersonsSandbox,
    private changeDetector: ChangeDetectorRef,
    private router: Router,
    ) {
    super(injector);
    this.registerEvents();
  }

  ngOnDestroy() {
    this.unregisterEvents();
  }

  /**
   * Unsubscribes from events
   */
  public unregisterEvents() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /*
  * Registers events
  */
  private registerEvents(): void {
    // Subscribes to product details
    this.subscriptions.push(this.personsSandbox.personDetails$.subscribe((person: any) => {
      if (person) {
        this.changeDetector.markForCheck();
        this.person = person;
        this.titlePage = `Person detail ${this.person ? `${this.person.firstName} ${this.person.surName}` : '' }`;
        this.setTitle(this.titlePage);    
      }
    }));
  }

  onToolbarAction = {
    onAddNew: (url): any => {     
      // hander function       
      this.router.navigate([url]);
    },
    onDelete: (id: any): any => {
      // hander function
      console.log(id);
    },
    onSave: (model: any): any => {
       // validate form
      let validateForm = this.formComponent.personForm.instance.validate();
      if (!validateForm.isValid) {
        let message = validateForm.brokenRules.map(o => o.message).join(', ');
        console.log(message);
        this.personsSandbox.notifyMessage(message, 'error');
      } else {
        this.personsSandbox.notifyMessage('Updated!', 'success');
        this.personsSandbox.loadPersons();
      }       
    }
  }
}
