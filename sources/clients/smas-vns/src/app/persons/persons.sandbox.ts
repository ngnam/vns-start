import { PUT } from './../shared/asyncServices/http/http.decorator';
import { Injectable }             from "@angular/core";
import { Store }                  from '@ngrx/store';
import { Subscription }           from "rxjs";
import { Sandbox }                from '../shared/sandbox/base.sandbox';
import { PersonsApiClient }       from './personsApiClient.service';
import * as store                 from '../shared/store';
import * as personsActions        from '../shared/store/actions/persons.action';
import * as personDetailsActions  from '../shared/store/actions/person-details.action';
import {
  Person,
  User
}                                 from '../shared/models';
import { UtilService }            from "../shared/utility";

@Injectable()
export class PersonsSandbox extends Sandbox {

  public persons$ = this.appState$.select(store.getPersonsData);
  public personsLoading$ = this.appState$.select(store.getPersonsLoading);
  public personDetails$ = this.appState$.select(store.getPersonDetailsData);
  public personDetailsLoading$ = this.appState$.select(store.getPersonDetailsLoading);
  public personAdded$ = this.appState$.select(store.getPersonAddNew);
  public statusAdding$ = this.appState$.select(store.getStatusAdding);

  private subscriptions: Array<Subscription> = [];

  constructor(
    protected appState$: Store<store.State>,
    private personsApiClient: PersonsApiClient,
    private utilService: UtilService,
  ) {
    super(appState$);
    this.registerEvents();
  }

  /**
* Loads persons from the server
*/
  public loadPersons(): void {
    this.appState$.dispatch(new personsActions.LoadAction())
  }

  /**
   * Loads person details from the server
   */
  public loadPersonDetails(id: any): void {
    this.appState$.dispatch(new personDetailsActions.LoadAction(id))
  }

  // ADD NEW
  public createPerson(person: any): void {
    this.appState$.dispatch(new personsActions.AddNewAction(new Person(person)));
  }

  /**
   * Dispatches an action to select person details
   */
  public selectPerson(person: Person): void {
    this.appState$.dispatch(new personDetailsActions.LoadSuccessAction(person))
  }

  public notifyMessage(messageTranslationCode: string, type: string = 'info', titleTranslationCode?: string): any {
    this.utilService.displayNotification(messageTranslationCode, type, titleTranslationCode);
  }

  /**
   * Unsubscribes from events
   */
  public unregisterEvents() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * Subscribes to events
   */
  private registerEvents(): void {
    // Subscribes to culture
    this.subscriptions.push(this.culture$.subscribe((culture: string) => this.culture = culture));

    this.subscriptions.push(this.loggedUser$.subscribe((user: User) => {
      if (user.isLoggedIn) this.loadPersons();
    }))    
  }
}
