import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { AuthGuard }                from '../shared/guards/auth.guard';
import { CanDeactivateGuard }       from '../shared/guards/canDeactivate.guard';
import { PersonsResolver }          from './persons.resolver';
import {
    PersonsComponent,
    PersonCreateComponent,
    PersonDetailsComponent
    }                               from  './pages';

const personsRoutes: Routes = [
    {
        path: 'persons',
        component: PersonsComponent,
        data: {
            title: 'Persons Manager',
            name:   'person-list'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'persons/create',
        component: PersonCreateComponent,
        data: {
            title: 'Create Person',
            name: 'person'
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'persons/:id',
        component: PersonDetailsComponent,
        resolve: {
            personDetails: PersonsResolver
        },
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(personsRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class PersonsRoutingModule { }
