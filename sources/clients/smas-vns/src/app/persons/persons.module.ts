import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import { BrowserModule }            from "@angular/platform-browser";
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
import {
  ReactiveFormsModule,
  NG_VALIDATORS,
  FormControl
}                                   from "@angular/forms";
import { RouterModule }             from '@angular/router';
import { PersonsRoutingModule }     from './persons-routing.module';
import { PersonsSandbox }           from './persons.sandbox';
import { PersonsApiClient }         from './personsApiClient.service';
import { PersonsService }           from './persons.service';
import { PersonsResolver }          from './persons.resolver';

import { TranslateModule }          from 'ng2-translate';

/**
 * Pages
 */
import {
  PersonsComponent,
  PersonCreateComponent,
  PersonDetailsComponent
  }                               from  './pages';

/**
 * Persons Modules
 */
import { PersonsComponentsModule } from './components';
import { PersonsContainersModule } from './containers';
import { ComponentsModule }        from '../shared/components';


@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    PersonsRoutingModule,
    PersonsComponentsModule,
    PersonsContainersModule,
    TranslateModule,
    BrowserModule,  
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations: [
    PersonsComponent,
    PersonCreateComponent,
    PersonDetailsComponent
  ],
  providers: [
    PersonsSandbox,
    PersonsService,
    PersonsApiClient,
    PersonsResolver
  ]
})
export class PersonsModule {}
