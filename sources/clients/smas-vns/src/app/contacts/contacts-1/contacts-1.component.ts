import {
    Injector,
    OnInit,
    Component,
    ChangeDetectionStrategy,
    OnDestroy
  }                           from '@angular/core';

import { BaseComponent }    from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ContactsSandbox } from '../contacts.sandbox';

  @Component({
    selector: 'vns-contact-one',
    templateUrl: `./contacts-1.component.html`,
    styleUrls: ['./contacts-1.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  
  export class ContactOneComponent extends BaseComponent implements OnInit, OnDestroy {

    private subscriptions:  Array<Subscription> = []; 
    
    constructor(
      injector: Injector,
      public contactsSandbox: ContactsSandbox,
      private route: ActivatedRoute
    ) {
      super(injector);      
    }

    ngOnInit() {
      this.titlePage = 'ContactOneComponent';
      this.setTitle(this.titlePage);     
    }    

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }    
}
  