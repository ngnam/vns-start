import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { AuthGuard }                from '../shared/guards/auth.guard';
import { CanDeactivateGuard }       from '../shared/guards/canDeactivate.guard';

import { ContactOneComponent } from './contacts-1/contacts-1.component';
import { ContactTwoComponent } from './contacts-2/contacts-2.component';

const routes: Routes = [
    {
        path: 'contacts/contact1',
        component: ContactOneComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'contacts/contact2',
        component: ContactTwoComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ContactsRoutingModule { }
