import {
    Injector,
    OnInit,
    Component,
    ChangeDetectionStrategy,
    OnDestroy
  }                           from '@angular/core';

import { BaseComponent }    from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ClassSandbox } from '../class.sandbox';

  @Component({
    selector: 'vns-class-management',
    templateUrl: `./class-management.component.html`,
    styleUrls: ['./class-management.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  
  export class ClassManagementComponent extends BaseComponent implements OnInit, OnDestroy {

    private subscriptions:  Array<Subscription> = []; 
    
    constructor(
      injector: Injector,
      public classSandbox: ClassSandbox,
      private route: ActivatedRoute
    ) {
      super(injector);      
    }

    ngOnInit() {
      this.titlePage = 'Danh sách lớp';
      this.setTitle(this.titlePage);     
    }    

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }    
}
  