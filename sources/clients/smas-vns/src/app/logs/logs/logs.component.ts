import {
  Injector,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  Component
} from '@angular/core';

import { BaseComponent } from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { LogsdSandbox } from '../logs.sandbox';

@Component({
  selector: 'vns-logs',
  templateUrl: `./logs.component.html`,
  styleUrls: ['./logs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class LogsComponent extends BaseComponent implements OnInit, OnDestroy {

  private subscriptions: Array<Subscription> = [];

  constructor(
    injector: Injector,
    public logsdSandbox: LogsdSandbox,
    private route: ActivatedRoute
  ) {
    super(injector);
  }

  ngOnInit() {
    this.titlePage = 'LogsComponent';
    this.setTitle(this.titlePage);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
