import { MessagesLogComponent } from './messages-log/messages-log.component';
import { MessagesFrameComponent } from './messages-frame/messages-frame.component';
import { MessagesToGroupComponent } from './message-to-group/message-to-group.component';
import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { AuthGuard }                from '../shared/guards/auth.guard';
import { CanDeactivateGuard }       from '../shared/guards/canDeactivate.guard';

const routes: Routes = [
    {
        path: 'messages/message-to-group',
        component: MessagesToGroupComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'messages/messages-frame',
        component: MessagesFrameComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'messages/messages-log',
        component: MessagesLogComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class MessagesRoutingModule { }
