import {
    Injector,
    OnInit,
    ChangeDetectionStrategy,
    OnDestroy,
    Component
  }                           from '@angular/core';

import { BaseComponent }    from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MessagesSandbox } from '../messages.sandbox';

@Component({
  selector: 'vns-message-to-group.',
  templateUrl: `./message-to-group.component.html`,
  styleUrls: ['./message-to-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
  
  export class MessagesToGroupComponent extends BaseComponent implements OnInit, OnDestroy {

    private subscriptions:  Array<Subscription> = []; 
    
    constructor(
      injector: Injector,
      public messagesSandbox: MessagesSandbox,
      private route: ActivatedRoute
    ) {
      super(injector);      
    }

    ngOnInit() {
      this.titlePage = 'MessagesToGroupComponent';
      this.setTitle(this.titlePage);     
    }    

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }    
}
  