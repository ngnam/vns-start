/**
 * Core Modules
 */
import { NgModule } from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { DevExtremeModule }         from 'devextreme-angular';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from 'ng2-translate';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

/**
 * Shared devextreme
 */
import { ComponentsModule }         from '../shared/components';
import { ContainersModule }         from '../shared/containers';

/**
 * Customs component, sandbox, routing modules
 */
import { MessagesSandbox } from './messages.sandbox';
import { MessagesRoutingModule } from './messages-routing.module';
import { MessagesLogComponent } from './messages-log/messages-log.component';
import { MessagesFrameComponent } from './messages-frame/messages-frame.component';
import { MessagesToGroupComponent } from './message-to-group/message-to-group.component';

@NgModule({
    imports: [       
        CommonModule,
        HttpClientModule,
        ComponentsModule,
        ContainersModule,
        TranslateModule,
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule,
        DevExtremeModule,
        MessagesRoutingModule
    ],
    declarations: [MessagesLogComponent, MessagesFrameComponent, MessagesToGroupComponent],
    providers: [
        MessagesSandbox
    ],
})

export class MessagesModule { }
