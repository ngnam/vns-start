import {
    Injector,
    OnInit,
    Component,
    ChangeDetectionStrategy,
    OnDestroy
  }                           from '@angular/core';

import { BaseComponent }    from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MessagesSandbox } from '../messages.sandbox';

@Component({
  selector: 'vns-messages-log',
  templateUrl: `./messages-log.component.html`,
  styleUrls: ['./messages-log.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
  
  export class MessagesLogComponent extends BaseComponent implements OnInit, OnDestroy {

    private subscriptions:  Array<Subscription> = []; 
    
    constructor(
      injector: Injector,
      public messagesSandbox: MessagesSandbox,
      private route: ActivatedRoute
    ) {
      super(injector);      
    }

    ngOnInit() {
      this.titlePage = 'MessagesLogComponent';
      this.setTitle(this.titlePage);     
    }    

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }    
}
  