import {
    Injector,
    OnInit,
    Component,
    ChangeDetectionStrategy,
    OnDestroy
  }                           from '@angular/core';

import { BaseComponent }    from '../../shared/components/base.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MessagesSandbox } from '../messages.sandbox';

  @Component({
    selector: 'vns-messages-frame',
    templateUrl: `./messages-frame.component.html`,
    styleUrls: ['./messages-frame.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  
  export class MessagesFrameComponent extends BaseComponent implements OnInit, OnDestroy {

    private subscriptions:  Array<Subscription> = []; 
    
    constructor(
      injector: Injector,
      public messagesSandbox: MessagesSandbox,
      private route: ActivatedRoute
    ) {
      super(injector);      
    }

    ngOnInit() {
      this.titlePage = 'MessagesFrameComponent';
      this.setTitle(this.titlePage);     
    }    

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }    
}
  