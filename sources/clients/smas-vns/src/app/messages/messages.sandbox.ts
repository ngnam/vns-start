import { Injectable } 	 from '@angular/core';
import { Router }        from '@angular/router';
import { Store }      	 from '@ngrx/store';
import { Subscription }  from "rxjs";
import { Sandbox } 			 from '../shared/sandbox/base.sandbox';

import {
  UtilService
}                        from '../shared/utility';


@Injectable()
export class MessagesSandbox {

  private subscriptions: Array<Subscription> = [];

  constructor(
    private router: Router,
    private utilService: UtilService,
  ) {}
    
}